##Organizing compute code and heap allocated reusable data buffers with classes in GC environment.##

*Problem will be elaborated on example of Bezier curve computer class BezierComputer in garbage collected environment of JavaScript VMs, using TypeScript. And I'll be talking in terms of BezierComputer class functionality.*

When I was writing computational heavy code I stumbled upon a problem of Garbage Collection(well, nothing new here right :) ). This was time when I was using functional code for computing Bezier curve points, something like:

```Typescript
var bezierPoint = computeBezierPoint(controlPointArray, 0.2)
```

One thing was clear that number of control points was mostly fixed but more importantly maximum number of control points was fixed or could be defined at the beginning of program. So, well, this is good candidate for pre allocated reusable buffers concept.

Fixed "High" buffer memory usage was less of a thing than high pause times due to GC and memory spikes.
Pre allocated buffer usage is one thing, other is managing computational code and memory buffers and dragging both to the points of usage in code, that get's nasty really nasty.

What I needed was two buffers, one for storing control points and another for computing interpolated(hence we're talking about Bezier curve) points, few other index/counter variables. So all this boiled down to writing class with interface like:

```Typescript
class Point2D
{
  x:number = 0.0
  y:number 0.0
  constructor(x?:number, y?:number)   
}
    
class BezierComputer
{ 
  private controlPointsBuffer:Array<Point2D>
  private computedPointsBuffer:Array<Point2D>
  private pushedControlPoints:number = 0  
 
  constructor(maxControlPointsCount:number)
   
  computeBezierPoint(out:Point2D, t:number)
 
  pushControlPoint(x:number, y:number) 
  resetControlPointsStackCounter()
}
```

So the consumer code can create BezierComputer object and than pass it on wherever needed, thus managing memory buffer and compute code together.

```
var bezierComputer1 = new BezierComputer(10)

bezierComputer1.pushControlPoint(0,0)
bezierComputer1.pushControlPoint(0,1)
bezierComputer1.pushControlPoint(1,1)

var bezPoint = new Point2D() 

bezierComputer1.computeBezierPoint(bezPoint, 0.2)
```
 
Using encapsulation from OOP is nothing new, as you've might guessed :) , but using that functionality for managing compute code and pre allocated(on heap) memory buffers I thought was worth noting.

You can go on and create some base class like ComputeNode which could define standard interface for all functionality like that, which could operate in fixed dynamic memory buffers and minimize GC as a consequence of reusing buffers and doing it in sane way :) .

**[Full example, see BezierComputer class](https://bitbucket.org/ivaneg/ivanets-framework/src/master/src/Math1.ts?at=master&fileviewer=file-view-default)**