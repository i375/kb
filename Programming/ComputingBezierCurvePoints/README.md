#Computing Bezier curve points.#

I needed to implement painting functionality with touch screen device magic.

The problem was no input device could give me "cursor" coordinates fast enough to form smooth lines.

I logged on to matrix of internet to find the solution to my problem.

What I needed was ability to compute points aligned to curved line defined by control points (as shown in Picture 1). Control points would be given to me by input device as a result of human interaction.

![Picture 1](res/Picture1.png)

*Picture 1: Green line is Bezier curve defined by **C1**,**C2**,**C3**,**C4** control points.*

That was Bezier curve computation algorithm, which actually is quite simple and easy after you tackle it but all I found was academic manuscripts from ancients druids of higher knowledge who didn't bother to trim down to solution, what actually mattered and leave higher order knowledge to those who were in need of it(which is fascinating on it's own).

But finally I figured it out and this is how its computed.

##The algorithm##

Let's call Bezier points computer function bezComputer with interface like:

```TypeScript
bezComputer( c1:Point2D, 
	         c2:Point2D, 
             c3:Point2D, 
             c3:Point2D, 
             t :float ):Point2D
```             
             
The **t** parameter values are in range of **[0..1]** where Bezier point at **t = 0** is at coordinate **C1** and Bezier point at **t=1** is at coordinate of **C4**, you get the idea, Bezier curve is represented like I'd say one dimensional space where you move from **0** to **1**.

So let's say we want to compute Bezier point **t=0.2** for control points **C1**,**C2**,**C3**,**C4**.

##Step 1:##

We collect 4[can be any other number >= 2] control points).

![Picture 2](res/Picture2.png)

*Picture 2: C1, C2, C3, C4 points are given to us by input device.*

Then we make function call like this:

```TypeScript
var bezPointAt_02 = bezComputer(C1, C2, C3, C4, 0.2)
```

##Step 2:##

We compute Linear interpolation(lerp) between point pairs **C1** and **C2**, **C2** and **C3**, **C3** and **C4** with linear interpolation coefficient equal to **0.2**.

And call those points **i1**, **i2**, **i3** as shown in **Picture 3**.

![Picture 3](res/Picture3.png)

*Picture 3: computing i1, i2, i3 points.*

Compute **i1**,**i2**,**i3**,**i4** points will look something like this in body of **bezComputer**:

```TypeScript
var i1 = lerp2D(c1, c2, t)
var i2 = lerp2D(c2, c3, t)
var i3 = lerp2D(c3, c4, t)
```

##Step 3:##

We compute linear interpolation between **i1** and **i2**, **i2** and **i3** point pairs with linear interpolation coefficient equal to **0.2** as shown on Picture **3**.

![Picture 4](res/Picture4.png)

*Picture 4: computing i4, i5 points.*

Computing **i4** and **i5** points will look like this in body of **bezComputer**:

```TypeScript
var i4 = lerp2D(i1, i2, t)
var i5 = lerp2D(i2, i3, t)
```

##Step 4:##

Now we compute i6 the final point and this is final step, cause it computes point which actually lies on Bezier curve defined with **C1**,**C2**,**C3**,**C4** control points as shown on in **Picture 5**.

![Picture 5](res/Picture5.png)

*Picture 5: computing i6 point.*

Computing i6 will look like this in bezComputer body:

```TypeScript
var i6 = lerp2D(i4, i5, t)
```

Well this is pretty much it, you change value of t from 0 to 1 with steps as small or big as you like and get points lying on Bezier curve defined with **C1**,**C2**,**C3**,**C4** control points.

![Picture 6](res/Picture6.png)

*Picture 6: Demonstrates i6 lying on Bezier curve.*

![Animation 1](res/Animation.gif)

*Animation 1: bezComputer in action.*

##Full source code (TypeScript):##

```TypeScript
/**
 * Basic 2D point class.
 */
class Point2D
{
    x:number
    y:number

    constructor(x:number, y:number)
    {
        this.x = x
        this.y = y 
    }
}

function bezComputer(c1:Point2D, c2:Point2D, c3:Point2D, c4:Point2D, t:number):Point2D
{ 
    var i1 = lerp2D(c1, c2, t)
    var i2 = lerp2D(c2, c3, t)
    var i3 = lerp2D(c3, c4, t)

    var i4 = lerp2D(i1, i2, t)
    var i5 = lerp2D(i2, i3, t)

    var bezPoint = lerp2D(i4, i5, t)

    return bezPoint
}

/**
 * Function for computing linear interpolation from p1 to p2.
 * 
 */
function lerp2D(p1:Point2D, p2:Point2D, t:number):Point2D
{
    var interpolatedPoint = new Point2D(0.0,0.0)

    interpolatedPoint.x = (1-t)*p1.x + t*p2.x
    interpolatedPoint.y = (1-t)*p1.y + t*p2.y

    return interpolatedPoint
}
```