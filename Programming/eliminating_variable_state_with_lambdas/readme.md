# Eliminating mutating state with lambdas

The idea is to tranform code in such a way that one can declare-assign (*initialize*) symbols in one step. Instead of declaring variable (*possibly initializing with default value, to be safe*), doing some conditional (*or other*) computation, reassigning variable and then using it.

> I'll be using TypeScript for code demos

### Imperative version of code with variable state (*mutating state*)

```TypeScript
declare function getEnemyStrength():number
declare function doAction(action:ActionType):void

enum ActionType
{
    Default,
    Shoot,
    Run,
    Hide
}

var action: ActionType = ActionType.Default

const enemyStrength = getEnemyStrength()

if (enemyStrength > 200)
{
    action = ActionType.Run
}
else if (enemyStrength > 100)
{
    action = ActionType.Hide
}
else if (enemyStrength > 0)
{
    action = ActionType.Shoot
}

doAction(action)
```
### Declarative code without mutating state, with lambda (*non mutating symbolic/declarative code*)

```TypeScript
declare function getEnemyStrength(): number
declare function doAction(action: ActionType): void

enum ActionType {
    Default,
    Shoot,
    Run,
    Hide
}

const action: ActionType = (() => {
    const enemyStrength = getEnemyStrength()

    if (enemyStrength > 200) {
        return ActionType.Run
    }
    else if (enemyStrength > 100) {
        return ActionType.Hide
    }
    else if (enemyStrength > 0) {
        return ActionType.Shoot
    }
    else
    {
        return ActionType.Default
    }

})()


doAction(action)
```

Declative syntax and sematics come with little performance cost, but if you have the budget it will lessen debuging time cost, cause you can analyze code like mathematical epxression (*logycally evaluating it*), without need to execute and runtime debug it. 

Although you can't fully eliminate runtime debugging, it's just about reducing need for it and making code more rearable and easy to follow, assuming one is familiar and comfortable with lambdas.

### Other handy example (*I'm not using JavaScript built in funtional features of array, just to demostrate the ideas*)

```TypeScript
declare function getPersons():Array<Person>

interface Person
{
    id: number
    firstName: string
    lastName :string
}

const persons: Array<Person> = getPersons()

const findById = 123

const personById = (() => {
    for (const personIndex in persons)
    {
        const person = persons[personIndex]

        if (person.id == findById)
        {
            return person
        }
    }

    return null
})()

if(personById != null)
{
    printPersonData(personById)
}
```

And one more c++ example (code is utter nonsense, just demostrates lambda usage)

```C++
#include <stdio.h>
#include <stdlib.h>

struct MyStruct
{
  int a;
  int b;
};

MyStruct func1(int a)
{
  return [](int a){
    if(a> 10)
    {
      return MyStruct{
        .a = 10, 
        .b = 20
      };      
    }
    else
    {
      return MyStruct{
        .a = 1,
        .b = 2
      };
    }

  }(a);
}

int main()
{
  auto ms = func1(rand());
  
  printf("hello %d %d \n", ms.a, ms.b);
}
```