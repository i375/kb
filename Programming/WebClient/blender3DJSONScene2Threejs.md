#ThreeJS: Load JSON Scene (from Blender3D)#

# Loading scene from server: #

```javascript
var loadedScene = null
var loader = new THREE.ObjectLoader()

loader.load('res/scene.json', (loadedObject) => { 
    scene.add(loadedObject)
})

...

renderer.render(loadedScene, camera)
```
 
# Loading scene from embeded resource(JSON object or string): #

```javascript
var loader = new THREE.ObjectLoader()

var parsed = loader.parse(Resources.scene1JSON) // scene1JSON can be either JSON object or String.

this.scene.add(parsed)
```
