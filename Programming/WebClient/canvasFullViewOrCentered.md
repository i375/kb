#HTML: Full view or centered canvas template#

```HTML
<style>
    #canvasContainer {
        width: 100%;
        height: 100%;
    }
    
    canvas {
        padding: 0;
        margin: auto;
        display: block;
     /* width: 1280px;
        height: 720px; */
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
    }
    
    body {
        overflow: hidden;
        margin: 0px;
        padding: 0px;
    }
</style>

<script src="bin/threejs/three.js"></script>
<script src="bin/main.js"></script>


<div id="canvasContainer">
    <canvas style="background-color:#eeeeee"></canvas>
</div>
```