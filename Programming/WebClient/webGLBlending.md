#WebGL Blending function#

Setting blend function: 

```
gl.blendFunc(sfactor, dfactor)
```

Source and destination color blending formula: 

```
                ⇩ color being drawn         ⇩ color already in frame buffer
color(RGBA) = ( sourceColor * sfactor ) + ( destinationColor * dfactor )    
```
*RBGA component values are between 0 and 1.*

Example: 
```
                     ⇩ sfactor                   ⇩ dfactor
gl.blendFunc(this.gl.SRC_ALPHA_SATURATE, this.gl.ONE)
``` 