#Object oriented code in C, or polymorphic structures#

```c

#include <stdlib.h>
#include <stdio.h>

// Base structure
typedef struct
{
    int a;
    int b;
    void(*Base_Virtual1)(struct Base* this);
}Base;


void Base_Virtual1(struct Base* this)
{
    printf("hello from base virtual function\n");
}

void Base_Init(Base* this);

Base* Base_Allocate()
{
    Base* this = malloc(sizeof(Base));
    Base_Init(this);

    return this;
}

void Base_Init(Base* this)
{
    this->Base_Virtual1 = Base_Virtual1;
}

int Base_GetSum(Base* this)
{
    return this->a + this->b;
}

void Base_Delete(Base* this)
{
    free(this);
}

// Derived structure
typedef struct
{
    Base base;
    int c;
}Derived;



Derived* Derived_Create()
{
    return malloc(sizeof(Derived));
}


void Derived_Virtual1(struct Base* this)
{
    printf("hello from derived virtual function\n");
}

void Derived_Init(Derived* this)
{
    this->base.Base_Virtual1 = Derived_Virtual1;
}

int Derived_GetSum(Derived* this)
{
    return this->base.a + this->base.b + this->c;

    //or non type safe way
    //return ((Base*)this)->a + ((Base*)this)->b + this->c;
}

void VirtualFunctionConsumer(Base* b)
{
    b->Base_Virtual1(b);
}

void Test()
{
    Base b;
    Base_Init(&b);

    Derived d;
    Derived_Init(&d);

    VirtualFunctionConsumer(&b);
    VirtualFunctionConsumer((Base*)(&d));

}

int main(void)
{

    Test();

    return 0;
}
```

Ouput will be:

```
hello from base virtual function
hello from derived virtual function
```