#Tools and resource:
 
#[C](https://en.wikipedia.org/wiki/C_(programming_language)) 

  - [X Macros](https://en.wikibooks.org/wiki/C_Programming/Preprocessor#X-Macros)

  - [C reference](http://en.cppreference.com/w/c)

  - [Criterion - Unit test](https://github.com/Snaipe/Criterion)

  - [Visual C++](https://msdn.microsoft.com/en-us/library/60k1461a.aspx)

     - [VisualC++ Team Blog](https://blogs.msdn.microsoft.com/vcblog/)
          
     - [_malloca (stack allocation)](https://msdn.microsoft.com/en-us/library/5471dc8s.aspx)

     - [Relocate intellisense .sdf database from project root](http://stackoverflow.com/a/7707083)

     - [Auto-Parallelization and Auto-Vectorization](https://msdn.microsoft.com/en-us/library/hh872235.aspx)

     - [Inline Functions - forceinline](https://msdn.microsoft.com/en-us/library/bw1hbe6y.aspx)

     - Linker

        - [Entry-Point Symbol](https://msdn.microsoft.com/en-us/library/f9t8842e.aspx)   

        - Preprocessor

            - [Preprocessor Ã¢â‚¬â€œ The Token Pasting (##) Operator](http://www.complete-concrete-concise.com/programming/c/preprocessor-the-token-pasting-operator)

            - [Predefined macros](https://msdn.microsoft.com/en-us/library/b0084kay.aspx) 
 
     - [**Code analysis:** SAL Static annotations language, to Reduce C/C++ Code Defects](https://msdn.microsoft.com/en-us/library/ms182032.aspx)

         - [Using PREfast for Static Code Analysis](http://www.codeproject.com/Articles/167588/Using-PREfast-for-Static-Code-Analysis)    

         - [SAL reference](https://msdn.microsoft.com/en-us/library/hh916382.aspx)

         - [Best practices](https://msdn.microsoft.com/en-us/library/jj159525.aspx)

  - [C interface API design guide](http://lucumr.pocoo.org/2013/8/18/beautiful-native-libraries/) 

  - [STDIO](https://en.wikipedia.org/wiki/C_file_input/output)     

  - [How to Uninstall Windows 10Ã¢â‚¬â„¢s Built-in Apps (and How to Reinstall Them)](http://www.howtogeek.com/224798/how-to-uninstall-windows-10s-built-in-apps-and-how-to-reinstall-them/) 
 
#Authorization:

 - [Digits - SMS authorization](https://get.digits.com/)
 
#Advertising:

 - [Where should you advertise a new iPhone Game app?](https://www.quora.com/Where-should-you-advertise-a-new-iPhone-Game-app)
 
 - [iOS game advertising package](https://www.buysellads.com/buy/bundle/id/41)
