#Windows(system)

shift+insert : paste

ctrl+insert : copy

win+x : main menu

win+e : explorer.exe

win+p : projection menu

ctrl+alt+up : landscape upward screen

ctrl+alt+down : landscape down screen

ctrl+alt+left : rotate counter clock wise screen

ctrl+alt+right : rotate clock wise screen

win+s : search

ctrl+z : undo

ctrl+y : redo

win+t : show, go through application thumbnails 

#Excel

ctrl+~ : expand formulas

f9 : recalculate

alt+f11 : toggle between excel and vba

#MS Word

ctrl+k : insert hyperlink

ctrl+d : set font

ctrl+e : switch between left/center alignment

ctrl+r : switch between center/right aligment

#Google spreadsheets

ctrl+r : recalculate

#Visual studio

ctrl+tab : quick navigation

ctrl+r, ctrl+r : rename

f12 : go to definition

shift+f12 : find all references

f5 : debug

ctrl+alt+break : break all threads at current point

shift+f5 : stop debugging

f10 : step over

f11 : step in

ctrl+k, ctrl+c : comment

ctrl+k, ctrl+u : uncomment

#Blender3D

ctrl+w (in edit mode) : choose editing type

Selected object + / : Edit selected object, isolated