#Knowledge base#

#Blender3D#

 - [Z Combine blurred and solid objects in Blender3D +](Blender3D/ZCombineblurredandsolidobjectsinBlender3D/)

 - [Auto Smooth Polygons, To hide triangulation](Blender3D/AutoSmoothPolygonsTohidetriangulation/)

 - [Add thickness to bezier curve +](Blender3D/add_bezier_curve_thickness_1478794261/add_thickness_to_bezier_curve_1478794694.svg)

 - [Using Blender3D as level editor with Unity3D, ThreeJS (or other)](Blender3D/using_blender_as_level_editor_with_unity3d/using_blender_as_level_editor_with_unity3d_1478934611.html)

 - To edit selected object, isolated, select object and press ```/```

#Programming#

 - [Eliminating mutating state with lambdas +](Programming/eliminating_variable_state_with_lambdas)

 - [Data structures and data types +](Programming/data_structures_and_types/data_structures_and_data_types.svg)

 - [Object oriented code in C, or polymorphic structures](https://bitbucket.org/9221145/object_oriented_c_project_sample)

 - **Swift language**

    - [Reduce array of structures to array of other type of structures in Swift language](Programming/SwiftLang/ReduceArrayOfStructuresToArrayOfotherTypeOfStructureInSwiftLanguage_1476938324/ReduceArrayOfStructuresToArrayOfotherTypeOfStructureInSwiftLanguage_1476938324.svg)

    - [Programatically instantiate controller in Swift language](Programming/SwiftLang/programaticallyInstantiateControllerFromSwiftLanguage/programaticallyInstantiateControllerFromSwiftLangauge_1477128918.svg)
  
 - [Organizing compute code and heap allocated reusable data buffers with classes in GC environment.](Programming/OrganizingComputeCodeAndHeapAllocatedReusableDataBuffersWithClassesInGCEnvironment)

 - [Computing Bezier curve points.](Programming/ComputingBezierCurvePoints)

 - [Computing square root](Programming/computing_square_root)

 - [Computing vector by knowing angle, without trigonometric functions](Programming/Geometry/vector_by_angle.xlsm)

 - [Game engine architecture 01](Programming/GameEngineArchitecture01) 
 
 - **Web Client**:
    - [ThreeJS: Load JSON Scene (from Blender3D).](Programming/WebClient/blender3DJSONScene2Threejs.md) 
    
    - [HTML: Full view or centered canvas template.](Programming/WebClient/canvasFullViewOrCentered.md) 
    
    - [WebGL Blending function. +](Programming/WebClient/webGLBlending.md)
    
 - **Git**:
    - [Basic commands +](Programming/Git/basicCommands.md)

#[Decision making and management](https://bitbucket.org/9221145/kb/src/master/DecisionMakingAndManagement/?at=master)

#Windows Command Line#

 - [Concatenate multiple text/javascript files. +](WindowsCommandLine/contacFilesOnWindows.md) 

 - [Windows cmd: Find files(s) with "where" command line tool. +](WindowsCommandLine/findFilesOnWindows.md) 

 - [PowerShell: Download file](https://bitbucket.org/9221145/kb/src/master/WindowsCommandLine/BitsTransfer.md?fileviewer=file-view-default)

 - [Misc.](WindowsCommandLine/misc.md)

 - [Command line reference +](https://technet.microsoft.com/en-us/library/cc754340(v=ws.11).aspx)
 
#MacOS#

 - [New terminal window at folder +](macOS/newTerminalAtFolder_1477323947.svg)

#[Shortcuts +](https://bitbucket.org/9221145/kb/src/master/shortcuts.md?fileviewer=file-view-default)


#External

##Unity3D

- [Shader introduction +](https://unity3d.com/learn/tutorials/topics/graphics/gentle-introduction-shaders)

- [HLSL Intrinsic Functions +](https://msdn.microsoft.com/en-us/library/windows/desktop/ff471376(v=vs.85).aspx)

- [HLSL Semantics +](https://msdn.microsoft.com/en-us/library/windows/desktop/bb509647(v=vs.85).aspx)

- [Unity WebGL Embedding API White Paper - 5.6 compatible](https://forum.unity3d.com/threads/new-unity-webgl-embedding-api-white-paper.430909/)

- [WebGL: Interacting with browser scripting](https://docs.unity3d.com/Manual/webgl-interactingwithbrowserscripting.html)