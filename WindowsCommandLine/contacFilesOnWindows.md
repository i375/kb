#Windows cmd: Concatenate multiple text/javascript files.#

To concatenate specific files: 
```
type file1.js file2.js > app.js
```

To concatenate all js(or any other extension) files from current directory/folder: 
```
type *.js > app.js
type ../somedir/*.js > ../release
```
