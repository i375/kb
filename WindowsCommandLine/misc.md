#Windows cmd: misc#

Openg URL:

```
start chrome http://localhost:8080
start http://localhost:8080
```

ON/OFF echo:

```
@echo on
@echo off
```

Open new cmd instance:

```
start
```

Turn screen saver on:

```
%systemroot%\system32\
scrnsave.scr /s
```

Generate energy report: 

```
powercfg -ENERGY
``` 

and open ```%systemroot%\system32\energy-report.html```